#include <iostream>

using namespace std;

class ReferenceCounter{
private:
	size_t count;
public:
	ReferenceCounter(size_t count = 0) : count(count) { }

		void add() {
			count++;
		}

		void substract() {
			count--;
		}

		const size_t operator++(){	// pre
			return ++count;
		}

		const size_t operator--(){
			return --count;
		}

		const size_t operator++(int){	// post
			size_t buf;
			buf = this->count;
			this->count = this->count + 1;
				return buf;
		}

		const size_t operator--(int){
			size_t buf;
			buf = this->count;
			this->count = this->count - 1;
				return buf;
		}

		const size_t howMany() const{
			return count;
		}


	~ReferenceCounter() { count = 0; }
};

template<typename T> class SmartPtr{
	T* data;
	ReferenceCounter* ref;
public:

	explicit SmartPtr(T* data = NULL) : data(data), ref(NULL){
		ref = new ReferenceCounter();
			if(data){
				++*ref;
			}
	}

	SmartPtr(const SmartPtr<T>& object){
		data = object.data;
		ref = object.ref;
		++*ref;
	}

	SmartPtr& operator=(SmartPtr<T>& object){
		data = object.data;
		ref = object.ref;
		++*ref;
	}

	T& operator*(){
		return *data;
	}

	T* operator->(){    
        return data;
    }

    ~SmartPtr(){
    	--*ref;
    	    	cout << *data << " Usuwam referencje" << endl;
    		if(ref->howMany() == 0){
    			cout <<*data <<" deleted"<<endl;
    			delete data;
    		}
 	}

};


int main(){
	SmartPtr<string> obiekt2;
	
	{
		SmartPtr<string> obiekt1(new string("Napis1"));
		obiekt2 = obiekt1;	// druga referencja na NAPIS1
	}	 // jezeli wychodzimy ze scope to referencja obiekt1 jest tracona zatem tylko obiekt2 wskazuje na Napis1

	cout << *obiekt2 << endl;

	return 0; 
}